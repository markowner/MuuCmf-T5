<?php

return [

    // 视图输出字符串内容替换,留空则会自动进行计算
    'view_replace_str'       => [
    	'__COMMON__'    => '/static/common',
        '__LIB__'       => '/static/common/lib',
        '__ZUI__'       => '/static/common/lib/zui',  
        '__JS__'    	=> '/static/articles/js',
        '__IMG__'       => '/static/articles/images',
        '__CSS__'       => '/static/articles/css',   
    ],
];